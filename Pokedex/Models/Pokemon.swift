//
//  Pokemon.swift
//  Pokedex
//
//  Created by Jean Manas on 10/24/21.
//

import Foundation
import UIKit

class Pokemon {
    let id: Int
    let name: String
    let hp: Int
    let atk: Int
    let def: Int
    let spA: Int
    let spD: Int
    let spe: Int
    let total: Int
    let typeI: Type
    
    var caught: Bool = false
    
    init(_ id: Int, _ name: String, _ hp: Int, _ atk: Int, _ def: Int, _ spA: Int,
         _ spD: Int, _ spe: Int, _ total: Int, _ typeI: String) {
        
        self.id = id
        self.name = name
        self.hp = hp
        self.atk = atk
        self.def = def
        self.spA = spA
        self.spD = spD
        self.spe = spe
        self.total = total
        self.typeI = Type(rawValue: typeI) ?? .Normal
    }
    
    enum `Type`: String {
        case Normal
        case Fire
        case Water
        case Grass
        case Electric
        case Ice
        case Fighting
        case Poison
        case Ground
        case Flying
        case Psychic
        case Bug
        case Rock
        case Ghost
        case Dark
        case Dragon
        case Steel
        case Fairy
    }
}
