//
//  DetailViewController.swift
//  Pokedex
//
//  Created by Jean Manas on 10/28/21.
//

import Foundation
import UIKit
import Kingfisher

class DetailViewController: UIViewController {
    var pokemon: Pokemon?
    
    @IBOutlet private var numLbl: UILabel?
    @IBOutlet private var nameLbl: UILabel?
    @IBOutlet private var hpLbl: UILabel?
    @IBOutlet private var atkLbl: UILabel?
    @IBOutlet private var defLbl: UILabel?
    @IBOutlet private var typeLbl: UILabel?
    @IBOutlet private var imageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populate()
    }
    
    private func populate() {
        guard let pokemon = pokemon else { return }
        numLbl?.text = "#\(pokemon.id)"
        nameLbl?.text = pokemon.name
        hpLbl?.text = "\(pokemon.hp)"
        atkLbl?.text = "\(pokemon.atk)"
        defLbl?.text = "\(pokemon.def)"
        typeLbl?.text = "\(pokemon.typeI.rawValue)"
        
        let url = URL(string: "https://cdn.traction.one/pokedex/pokemon/\(pokemon.id).png")
        imageView?.kf.setImage(with: url)
    }
}
