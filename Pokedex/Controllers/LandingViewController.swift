//
//  LandingViewController.swift
//  Pokedex
//
//  Created by Jean Manas on 11/2/21.
//

import Foundation
import UIKit

/// An example of a UIViewController without Storyboard references
class LandingViewController: UIViewController {
    lazy private var genIBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("View Gen I", for: .normal)
        return btn
    }()
    
    lazy private var genIIBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("View Gen II", for: .normal)
        
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout()
        style()
        addTargets()
    }
    
    private func layout() {
        let sView = UIStackView()
        sView.axis = .vertical
        sView.spacing = 16
        sView.alignment = .fill
        sView.distribution = .fill
        
        sView.addArrangedSubview(genIBtn)
        sView.addArrangedSubview(genIIBtn)
        
        genIBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        genIIBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        view.addSubview(sView)
        sView.translatesAutoresizingMaskIntoConstraints = false
        
        sView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        sView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 45).isActive = true
        sView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -45).isActive = true
    }
    
    private func style() {
        view.backgroundColor = .white
        
        genIBtn.style()
        genIIBtn.style()
    }
    
    private func addTargets() {
        genIBtn.addTarget(self, action: #selector(didTapGenI), for: .touchUpInside)
        genIIBtn.addTarget(self, action: #selector(didTapGenII), for: .touchUpInside)
    }
    
    @objc private func didTapGenI() {
        print("did tap gen i")
        let vc = UIStoryboard.initVC(type: PokemonViewController.self)
        vc.data = GenIData().list()
        vc.tableDelegate = GenITableDelegate()
        
        navigationController?.show(vc, sender: self)
    }
    
    @objc private func didTapGenII() {
        print("did tap gen ii")
        let vc = UIStoryboard.initVC(type: PokemonViewController.self)
        vc.data = GenIIData().list()
        vc.tableDelegate = GenIITableDelegate()
        
        navigationController?.show(vc, sender: self)
    }
}

fileprivate extension UIButton {
    func style() {
        backgroundColor = .systemBlue
        setTitleColor(.white, for: .normal)
        layer.cornerRadius = 8
    }
}
