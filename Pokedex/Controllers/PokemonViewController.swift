//
//  ViewController.swift
//  Pokedex
//
//  Created by Jean Manas on 10/24/21.
//

import UIKit

protocol PokemonTableDelegate {
    func didSelect(pokemon: Pokemon, navigationController: UINavigationController)
}

class PokemonViewController: UIViewController {
    
    /// Inject the data to be displayed from an external source
    var data: [Pokemon] = []
    
    /// Inject the delegate to perform 
    var tableDelegate: PokemonTableDelegate?

    @IBOutlet var pokemonTable: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pokemonTable?.dataSource = self
        pokemonTable?.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DetailViewController {
            vc.pokemon = sender as? Pokemon
        }
    }
    
    func navigateUsingSegue(pokemon: Pokemon) {
        performSegue(withIdentifier: "showDetail", sender: pokemon)
    }
    
    func navigateUsingCode(pokemon: Pokemon) {
        guard let vc = storyboard?.instantiateViewController(identifier: "DetailViewController") as? DetailViewController
        else { return }
        vc.pokemon = pokemon
        navigationController?.present(vc, animated: true, completion: nil)
    }
}

extension PokemonViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let pokemon = data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! PokemonTableViewCell
        
        cell.set(p: pokemon)
        cell.delegate = self
        return cell
    }
}

extension PokemonViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Did select pokemon \(data[indexPath.row].name)")
        tableDelegate?.didSelect(pokemon: data[indexPath.row], navigationController: navigationController!)
    }
}

extension PokemonViewController: PokemonTableViewCellDelegate {
    func markAsCaught(pokemon: Pokemon) {
        print("Catching pokemon \(pokemon.name)")
        pokemon.caught = true
        pokemonTable?.reloadData()
    }
    
    func markAsReleased(pokemon: Pokemon) {
        print("Releasing pokemon \(pokemon.name)")
        pokemon.caught = false
        pokemonTable?.reloadData()
    }
}
