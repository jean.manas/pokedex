//
//  TableDelegates.swift
//  Pokedex
//
//  Created by Jean Manas on 11/2/21.
//

import Foundation
import UIKit

/// The classes responsible for reacting to a `didSelect` pokemon event
class GenITableDelegate: PokemonTableDelegate {
    func didSelect(pokemon: Pokemon, navigationController: UINavigationController) {
        let vc = UIStoryboard.initVC(type: DetailViewController.self)
        navigationController.show(vc, sender: nil)
    }
}

class GenIITableDelegate: PokemonTableDelegate {
    func didSelect(pokemon: Pokemon, navigationController: UINavigationController) {
        let vc = UIStoryboard.initVC(type: DetailViewController.self)
        navigationController.present(vc, animated: true, completion: nil)
    }
}
