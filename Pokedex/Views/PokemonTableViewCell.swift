//
//  PokemonTableViewCell.swift
//  Pokedex
//
//  Created by Jean Manas on 10/26/21.
//

import Foundation
import UIKit

protocol PokemonTableViewCellDelegate: AnyObject {
    func markAsCaught(pokemon: Pokemon)
    func markAsReleased(pokemon: Pokemon)
}

class PokemonTableViewCell: UITableViewCell {
    private var pokemon: Pokemon?
    weak var delegate: PokemonTableViewCellDelegate?
    
    @IBOutlet private var numLabel: UILabel?
    @IBOutlet private var nameLabel: UILabel?
    @IBOutlet private var hpLabel: UILabel?
    @IBOutlet private var atkLabel: UILabel?
    @IBOutlet private var defLabel: UILabel?
    @IBOutlet private var typeLabel: UILabel?
    
    @IBOutlet private var catchBtn: UIButton?
    @IBOutlet private var releaseBtn: UIButton?
    
    @IBAction func didTapCatch() {
        guard let p = pokemon else { return }
        delegate?.markAsCaught(pokemon: p)
        print("did catch \(p.name)")
    }
    
    @IBAction func didTapRelease() {
        guard let p = pokemon else { return }
        delegate?.markAsReleased(pokemon: p)
        print("did release \(p.name)")
    }
    
    func set(p: Pokemon) {
        
        pokemon = p
        
        atkLabel?.text = "ATK: \(p.atk)"
        hpLabel?.text = "HP: \(p.hp)"
        defLabel?.text = "DEF: \(p.def)"
        nameLabel?.text = "\(p.name)"
        numLabel?.text = "#\(p.id)"
        typeLabel?.text = "\(p.typeI.rawValue)"
        
        catchBtn?.isHidden = p.caught
        releaseBtn?.isHidden = !p.caught
    }
}
