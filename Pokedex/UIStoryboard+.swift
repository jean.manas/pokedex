//
//  UIStoryboard+.swift
//  Pokedex
//
//  Created by Jean Manas on 11/2/21.
//

import Foundation
import UIKit

// PokemonViewController.identifier = "PokemonViewController"
extension UIViewController {
    static var identifier: String {
        return String(describing: self)
    }
}

extension UIStoryboard {
    static func initVC<T: UIViewController>(type: T.Type) -> T {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        return sb.instantiateViewController(identifier: T.self.identifier) as! T
    }
}
